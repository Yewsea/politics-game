import Voter
import Seat
import Party
import time
from utils import generateRandomWithWeightedMean
from collections import defaultdict

govHealth = 95
timer = 0

# voters = [Voter.Voter(x, 50, 50, 50) for x in range(100)]
#
# for voter in voters:
#     voter.setRandomAge()
#
# while len(voters) > 0:
#     time.sleep(0.25)
#     totalAge = 0
#     totalHealth = 0
#     for voter in voters:
#         voter.ageUp(govHealth)
#         totalAge += voter.returnAge()
#         totalHealth += voter.health
#
#     timer += 1
#     survivingVoters = [voter for voter in voters if not voter.isDead]
#     voters = survivingVoters
#
# print("100 dead in ", timer//12, " years")

seats = []
parties = []

seats.append(Seat.Seat("First Seat", 1, 80, 75, 60, 150))
seats.append(Seat.Seat("Second Seat", 2, 15, 50, 65, 90))
seats.append(Seat.Seat("Third Seat", 3, 60, 20, 20, 65))
seats.append(Seat.Seat("Fourth Seat", 4,  40, 35, 50, 25))
seats.append(Seat.Seat("Fifth Seat", 5, 50, 50, 50, 0))
seats.append(Seat.Seat("Sixth Seat", 6, 20, 25, 45, 120))
seats.append(Seat.Seat("Seventh Seat", 7, 50, 55, 45, -60))
seats.append(Seat.Seat("Eighth Seat", 8, 30, 80, 35, 40))
seats.append(Seat.Seat("Ninth Seat", 3, 10, 90, 90, 80))
seats.append(Seat.Seat("Tenth Seat", 3, 90, 12, 10, -10))

parties.append(Party.Party("Conservatives", 70, 60, 60))
parties.append(Party.Party("Social Democrats", 30, 55, 65))
parties.append(Party.Party("Liberals", 55, 15, 20))

pollingFigures = {party.name: 0 for party in parties}

classList = defaultdict(int)
for seat in seats:
    seat.addVoters()
    seat.addParties(parties)
    print(seat.name, seat.returnPoll())
    for party in seat.returnPoll():
        pollingFigures[party] += seat.returnPoll()[party]
    for voter in seat.voters:
        classList[voter.returnSocialClass()] += 1


print(pollingFigures)
print(classList)

