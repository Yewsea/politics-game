from Voter import Voter
from random import choice
from collections import defaultdict
from utils import generateRandomWithWeightedMean


class Seat:

    def __init__(self, name, seatId, econLean, authLean, natLean, spread):
        self.name = name
        self.seatId = seatId
        self.econLean = econLean
        self.authLean = authLean
        self.natLean = natLean
        self.spread = spread
        self.jobs = 0
        self.voters = []
        self.parties = []

    def addVoters(self):
        for v in range(200):
            econ = generateRandomWithWeightedMean(self.econLean, self.spread)
            auth = generateRandomWithWeightedMean(self.authLean, self.spread)
            nat = generateRandomWithWeightedMean(self.natLean, self.spread)
            self.voters.append(Voter(v, econ, auth, nat))

    def addParties(self, partyList):
        self.parties += partyList

    def returnPoll(self):
        pollResult = defaultdict(int)
        for x in range(20):
            voter = choice(self.voters)
            pollResult[voter.chooseParty(self.parties)] += 1

        return pollResult

    def returnWinner(self):
        pass
