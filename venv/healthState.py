from enum import Enum

class HealthState(Enum):
    Healthy     = 0
    Sick        = 1
    Critical    = 2
    Terminal    = 3
