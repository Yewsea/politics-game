from enum import Enum

class SocialClass(Enum):
    Elite            =  0
    UpperMiddle      =  1
    Middle           =  2
    LowerMiddle      =  3
    SkilledWorking   =  4
    UnskilledWorking =  5
    UnderClass       =  6