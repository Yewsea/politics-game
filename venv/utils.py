import random

def generateRandomWithWeightedMean(mean, weight):
    number = random.randint(0, 100)
    border = 100 if number > mean else 0
    distance = abs(border - number)
    weightedShift =  distance * (weight/100)
    if number > mean:
        number = number - weightedShift if number -weightedShift > mean else mean
    if number < mean:
        number = number + weightedShift if number + weightedShift < mean else  mean

    return number

def setRandomSocialClass(econ):
    if econ < 20:
        options = [6,6,6,6,5,5,5,5,4,4]
    elif econ < 40:
        options = [6,6,5,5,4,4,4,4,3,3]
    elif econ < 60:
        options = [5,4,4,4,3,3,3,3,2,2]
    elif econ < 80:
        options = [4,4,3,3,3,2,2,2,2,1]
    else:
        options = [3,3,2,2,2,2,2,1,1,0]
    socClassInt = random.choice(options)
    return socClassInt