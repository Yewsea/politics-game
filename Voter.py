from random import randint, random
from healthState import HealthState
from SocialClass import SocialClass
from utils import setRandomSocialClass


class Voter:

    def __init__(self, voterId, econLean, authLean, natLean):
        self.voterId = voterId
        self.econLean = econLean
        self.authLean = authLean
        self.natLean = natLean
        self.age = 0  # age in months
        self.health = 100.0
        self.maxHealth = 100.0
        self.sicknessLevel = HealthState.Healthy
        self.income = 0
        self.money = 0
        self.socialClass = SocialClass(setRandomSocialClass(self.econLean))
        self.happiness = 0
        self.inEducation = False
        self.hasJob = False
        self.hasHome = True
        self.isDead = False

    def returnAge(self):
        return self.age // 12

    def returnSocialClass(self):
        return self.socialClass

    def setRandomAge(self):
        self.age = randint(0, 900)
        if  4 <= self.age <= 16:
            self.inEducation = True

    def chooseParty(self, partyList):
        desiredParty = ''
        topPartyScore = 200

        for party in partyList:
            econ = abs(self.econLean - party.econLean)
            auth = abs(self.authLean - party.authLean)
            nat = abs(self.natLean - party.natLean)
            partyScore = (econ**2 + auth**2 + nat**2)**0.5
            if partyScore < topPartyScore:
                topPartyScore = partyScore
                desiredParty = party.name

        return desiredParty

    def ageUp(self, govHealth):
        self.age += 1
        self.healthDecrease(govHealth)
        if self.age > 420:
            self.econLean += 0.03
        if self.age > 360:
            self.authLean += 0.03
        if self.age > 480:
            self.natLean += 0.03

    def healthDecrease(self, govHealth):
        self.calculateSickness(govHealth)
        scale = (1.5*(1-(govHealth/100)))+1
        healthBase = (((self.age/250)**3)+1)/10
        self.health -= healthBase * scale * (1 + (self.sicknessLevel.value/10))
        self.health += (govHealth/10) - (self.sicknessLevel.value * 1.5)

        if self.health > self.maxHealth:
            self.health = self.maxHealth
        elif self.health < 0:
            self.calculateDying();

    def calculateDying(self):
        deathOdds = abs(self.health)/100 * (self.sicknessLevel.value + 1)
        deathChance = random()
        if deathChance < deathOdds:
            self.isDead = True
            print("Voter ", self.voterId, " has died at age ", self.returnAge())
        else:
            self.health = 0

    def calculateSickness(self, govHealth):
        sicknessOdds = (100-govHealth)/100.0
        sicknessChance = random()

        if self.sicknessLevel is HealthState.Healthy:
            if sicknessChance < (sicknessOdds/250) + 0.0001:
                self.sicknessLevel = HealthState.Terminal
                self.maxHealth = 30
                print("Healthy Voter ", self.voterId, " has gotten a terminal illness at age ", self.returnAge())
            elif sicknessChance < (sicknessOdds/100) + 0.0005:
                self.sicknessLevel = HealthState.Critical
                self.maxHealth = 50
                print("Healthy Voter ", self.voterId, " has gotten a critical illness at age ", self.returnAge())
            elif sicknessChance < sicknessOdds/10:
                self.sicknessLevel = HealthState.Sick
                self.maxHealth = 80

        elif self.sicknessLevel is HealthState.Sick:
            if sicknessChance * 0.2 > sicknessOdds:
                self.sicknessLevel = HealthState.Healthy
                self.maxHealth = 100
            elif sicknessChance < (sicknessOdds/150) + 0.0001:
                self.sicknessLevel = HealthState.Terminal
                self.maxHealth = 30
                print("Sick Voter ", self.voterId, " has dropped to terminal illness at age ", self.returnAge())
            elif sicknessChance < (sicknessOdds/25) + 0.0001:
                self.sicknessLevel = HealthState.Critical
                self.maxHealth = 50
                print("Sick Voter ", self.voterId, " has dropped to critical illness at age ", self.returnAge())

        elif self.sicknessLevel is HealthState.Critical:
            if sicknessChance * 0.07 > sicknessOdds:
                self.sicknessLevel = HealthState.Healthy
                self.maxHealth = 90
                print(sicknessChance * 0.07, sicknessOdds)
                print("Critical Voter ", self.voterId, " has recovered from their illness at age ", self.returnAge())
            elif sicknessChance * 0.12 > sicknessOdds:
                self.sicknessLevel = HealthState.Sick
                self.maxHealth = 70
                print(sicknessChance * 0.12, sicknessOdds)
                print("Critical Voter ", self.voterId, " is recovering from their illness at age ", self.returnAge())
            elif sicknessChance < (sicknessOdds/50) + 0.001:
                self.sicknessLevel = HealthState.Terminal
                self.maxHealth = 30
                print("Critical Voter ", self.voterId, "'s illness is now terminal at age ", self.returnAge())

    def changeSocialClass(self, num):
        level = self.socialClass.value
        newNum = level + num
        self.socialClass = SocialClass(newNum)



